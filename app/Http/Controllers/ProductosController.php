<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productos;

class ProductosController extends Controller
{
    public function index()
    {
        $productos = '
                    [
                    {"id":1,"imagen":"http://127.0.0.1:8000/imagenes/01.jpg","nombre":"Galletas de cordero y Arroz Laika By Rausch","precio_oferta":1,"precio":5942,"oferta":5732,"peso_bool":1,"peso":"150GR","paquete_bool":0,"grande_bool":0,"mediano_bool":0,"min_bool":0},
                    {"id":2,"imagen":"http://127.0.0.1:8000/imagenes/02.jpg","nombre":"Salvaje Snacks Efecto Calmante","precio_oferta":1,"precio":5653,"oferta":5732,"peso_bool":1,"peso":"150GR","paquete_bool":0,"grande_bool":0,"mediano_bool":0,"min_bool":0},
                    {"id":3,"imagen":"http://127.0.0.1:8000/imagenes/03.jpg","nombre":"Salvaje Snacks Pollo, Cordero y Salmón","precio_oferta":1,"precio":4803,"oferta":4633,"peso_bool":1,"peso":"170GR","paquete_bool":0,"grande_bool":0,"mediano_bool":0,"min_bool":0},
                    {"id":4,"imagen":"http://127.0.0.1:8000/imagenes/04.jpg","nombre":"Cozy Hug Comodísimos","precio_oferta":1,"precio":238000,"oferta":229600,"peso_bool":0,"peso":0,"paquete_bool":1,"grande_bool":1,"mediano_bool":1,"min_bool":0 }
                    ]
                    ';
        return json_decode($productos);
    }
    
}
